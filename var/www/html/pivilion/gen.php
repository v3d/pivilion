<?php
ob_start(); // start trapping output
$banana = @$_POST['banana'];
$title = @$_POST['title'];
$description = @$_POST['description'];
$images_array = $_FILES['mytest'];
$slides_array = @$_POST['imgs'];
$slides_autors = @$_POST['author'];
$slides_desc = @$_POST['work_desc'];
$color = @$_POST['color'];

function rearrange($arr)
{
    foreach ($arr as $key => $all)
    {
        foreach ($all as $i => $val)
        {
            $new[$i][$key] = $val;
        }
    }
    return $new;
}

$myfiles = array();
// $file_ary = $_FILES['mytest'];
if (isset($_FILES['mytest']))
{
    foreach (rearrange($images_array) as $file)
    {
        //echo $file['name'];
        

        $target_dir = "gen/upload/";
        usleep(1);
        $target_file = $target_dir . str_replace(",", "", str_replace(".", "", number_format(microtime(true) , 4))) . '.' . pathinfo($file['name'], PATHINFO_EXTENSION);

        $uploadOk = 1;
        $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
        // Check if image file is a actual image or fake image
        //echo $imageFileType;
        // $check = getimagesize($file['tmp_name']);
        // if($check !== false) {
        //    // echo "File is an image - " . $check["mime"] . ".";
        //     $uploadOk = 1;
        // } else {
        //   //  echo "File is not an image.";
        //     $uploadOk = 0;
        // }
        // Check if file already exists
        if (file_exists($target_file))
        {
            //    echo "Sorry, file already exists.";
            $uploadOk = 0;
        }
        // Check file size
        /*if ($file["size"] > 500000) {
        echo "Sorry, your file is too large.";
        $uploadOk = 0;
        }*/
        // Allow certain file formats
        if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" && $imageFileType != "webm" && $imageFileType != "ogg")
        {
            //    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
            $uploadOk = 0;
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0)
        {
            array_push($myfiles, "");
            //   echo "Sorry, your file was not uploaded.";
            // if everything is ok, try to upload file
            
        }
        else
        {
            if (move_uploaded_file($file['tmp_name'], $target_file))
            {

                $file['tmp_name'] = $target_file;
                array_push($myfiles, $target_file);
                //echo "The file ". basename( $file['name']). " has been uploaded.";
                
            }
            else
            {
                array_push($myfiles, "");
                // echo "Sorry, there was an error uploading your file.";
                
            }
        }
    }
}
?>


<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Pivilion | <?php echo $title; ?> </title>
    <meta name="author" content="pivilion.net | curated by <?php echo $banana; ?>" />
    <meta name="description" content="Pivilion darknet gallery" />
    <meta name="Resource-type" content="Document" />
    <style type="text/css">
    .pace .pace-progress {
      background: #<?php echo $color; ?>!important;
      position: fixed;
      z-index: 2000;
      top: 0;
      right: 100%;
      width: 100%;
      height: 5px;
    }
    </style>
    <link rel="stylesheet" type="text/css" href="css/jquery.fullPage.css" />
    <!--[if IE]>
        <script type="text/javascript">
             var console = { log: function() {} };
        </script>
    <![endif]-->
    <script src="js/jquery.min.js"></script>
    <script src="js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="js/jquery.fullPage.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#fullpage').fullpage({
                verticalCentered: true,
            });

        $('.arrowUp').click(function(){
          $.fn.fullpage.moveSectionUp();
            });

        $('.arrowDown').click(function(){
        $.fn.fullpage.moveSectionDown();
            });
   });
    </script>
</head>
<body>
<noscript>
<div class="section">
<p>This site requires JavaScript. It will not work properly if you have it disabled. If you are seeing this, you are most likely using Orfox. <br>
To enable javascript do
<br>
Menu -> NoScript -> Tick the boxes for the sites.
<br>
or
<br>
Menu -> Tools -> Add-ons -> NoScript -> Enable/Disable/Configure
<br>
<strong>Beware: whitelists and user-specific configurations are probably fingerprintable and enabling javascript at all might expose functionality that isn't fully protected against yet. Orfox is still not fully implementing the Tor Browser design spec! So consider returning to default settings once you are done viewing this gallery!</strong>
 </p> 
</div>
</noscript>
<?php
if (isset($_POST['togetherjs']) && $_POST['togetherjs'] == 'Yes')
{
    echo '<script>
      TogetherJSConfig_findRoom = {prefix: "piviliontestrun2", max: 50};
      TogetherJSConfig_autoStart = true;
      TogetherJSConfig_suppressJoinConfirmation = true;
      TogetherJSConfig_storagePrefix = "tst_pi";
    </script>
<script src="https://togetherjs.com/togetherjs-min.js"></script>';
}
else
{
    echo " ";
}

?>



<script type="text/javascript" src="js/pace.js"></script>
<!--<div id="header">Pivilion</div>
<div id="footer">By <3+D</div>-->
<!--end of section -->
<div id="fullpage">
    <div class="section" id="section0">
        <h1><?php echo $title; ?></h1>
        <p><?php echo $description; ?><p>
         
    
    <?php
$myfiles = preg_replace("/gen\//", "", $myfiles);

//var_dump($myfiles);
$cnt = 1;
foreach ($slides_array as $imageVal)
{
    // echo $cnt;
    

    if ($imageVal == '--SECTION--')
    {
        $sld = 1;
        echo '</div>';
        echo '<div class="section" id="section' . $cnt . '">';
        $cnt++;
        //next($myfiles);
        
    }
    else
    {
        $ext = "";
        $ext = explode(".", current($myfiles));
        $format = $ext[sizeof($ext) - 1];
        switch ($format)
        {
            case "webm":
                echo '<div class="slide" id="slide' . $sld . '">
						<video src="' . current($myfiles) . '" data-autoplay="1" loop="1" controls ="1"></video>
						<div>' . $imageVal . '</div><div>' . current($slides_autors) . '</div><div>' . current($slides_desc) . '</div>
						</div>';
            break;

            case "ogg":
                echo '<div class="slide" id="slide' . $sld . '">
						<audio src="' . current($myfiles) . '" data-autoplay="1" controls="1" loop="1"></audio>
						<div>' . $imageVal . '</div><div>' . current($slides_autors) . '</div><div>' . current($slides_desc) . '</div>
						</div>';
            break;

            default:
                echo '<div class="slide" id="slide' . $sld . '">
						<img src="' . current($myfiles) . '" />
						<div>' . $imageVal . '</div><div>' . current($slides_autors) . '</div><div>' . current($slides_desc) . '</div>
						</div>';
            break;
        }
        next($slides_desc);
        next($slides_autors);
        next($myfiles);

        $sld++;

    }

}

?>
<!--<div class="arrowUp"></div>
<div class="arrowDown"></div>-->
</body>
</html>
<?php
$source = "skeleton/";
$dest = "/var/www/html/pivilion/gen";

mkdir($dest, 0755);
foreach ($iterator = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($source, \RecursiveDirectoryIterator::SKIP_DOTS) , \RecursiveIteratorIterator::SELF_FIRST) as $item)
{
    if ($item->isDir())
    {
        mkdir($dest . DIRECTORY_SEPARATOR . $iterator->getSubPathName());
    }
    else
    {
        copy($item, $dest . DIRECTORY_SEPARATOR . $iterator->getSubPathName());
    }
}
?>
<?php
$torname = file_get_contents('/home/pi/pivilion/torname');
$tornamemode = preg_replace('/\s+/', '', $torname);
$output = ob_get_contents(); // get contents of trapped output
//write to file, e.g.
$newfile = "/var/www/html/pivilion/gen/index.php";
$file = fopen($newfile, "w");
fwrite($file, $output);
fclose($file);
ob_end_clean(); // discard trapped output and stop trapping

?>
<a href="http://<?php echo $torname; ?>/" target="_blank">Gallery is here!</a>
