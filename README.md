[Manual](https://gitlab.com/hacklab01/pivilion/wikis/pivilion-manual-setup)

Pivilion is a decentralizing open source darknet web server project and gallery management software for the creation of autonomous & uncensored digital media art online galleries. It runs on low cost Raspberry Pi hardware and is built on top of Raspbian GNU/Linux with a server and Tor networking built in, utilizing the Tor network to host exhibitions out of the box.

Each Pivilion device receives a Tor onion domain automatically the first time it’s activated. The system provides the user with a CMS for publishing multimedia or websites within a gallery. It’s designed so that the author-curator can use any network (even public networks behind firewalls) to host an online exhibition.

Pivilion is primarily a long-term, open-ended new media art project propagating autonomous modes of art in the darknet, and is continuously including a large number of artists and cultural workers in its creation.

The documentation and the announcement of global #pivilion_dot events are available via the central website, hosted both on clearnet and as a hidden service on the Tor network, while the entire open source system is available via git.

Project website: [pivilion.net](https://pivilion.net/)

Required hardware:

    - Raspberry Pi with WiFi
    - min 8 GB SD card
    - battery or power adapter
